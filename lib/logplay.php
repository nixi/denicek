<?php
class logplay
{

	protected $db;

	// dependency injection
	function __construct($db){
		$this->db = $db;
	}

	function auth($trip){
		//$trip = password_hash($trip, PASSWORD_DEFAULT);
		$res = $this->db->query("
			select id, trip from users
		",[]);
		while($row = $res->fetch(\PDO::FETCH_ASSOC)){
			if(password_verify($trip, $row["trip"])){ return $row["id"]; }
		}
		return false;
	}

	function append($arr){
		$this->db->query("
			insert into log
			(uid,loc,val,desc,date)
			values (?,?,?,?,?)
		",$arr);
	}

	// select last n rows according to $limit
	function get_records($limit):string{
		$cont = "";
		$res = $this->db->query("
			select log.date date, log.loc loc, users.name who, log.val val, log.desc desc from log
			join users on uid = users.id
			order by date desc
			limit ?	
		",[$limit]);

		while($row = $res->fetch(\PDO::FETCH_ASSOC)){
			$row["date"] = date("m-d",strtotime($row["date"]));
			$row["val"] = round($row["val"],0);
			$row["desc"] = (empty($row["desc"]) ? "..." : $row["desc"]);
			$cont .= "
				<tr>
					<td>{$row["loc"]}</td>
					<td>{$row["date"]}</td>
					<td>{$row["who"]}</td>
					<td>{$row["val"]} czk</td>
				</tr>
				<tr>
					<td colspan=\"4\">{$row["desc"]}</td>
				</tr>
			";
		}

		return $cont;
	}

	// return the total difference (of two users)
	function get_diff():string{
		$final = array( "sum" => 0 );
		$res = $this->db->query("
			select users.name name, sum(log.val) sum from users
			join log on log.uid = users.id
			group by users.id
			order by sum
		",[]);

		while($row = $res->fetch(\PDO::FETCH_ASSOC)){
			$final["name"] = $row["name"];
			$final["sum"] = abs($final["sum"] - $row["sum"]);
		}
		$final["sum"] = round($final["sum"],0);
		$cont = "
			<td>{$final["name"]}</td>
			<td><span class=\"val-pos\">+{$final["sum"]}</span> czk</td>
		";

		return $cont;
	}

	// return the remainder of funds for current month (for each user)
	function get_remainder():string{
		$cont = "";
		$res = $this->db->query("
			select users.name name, users.border border, users.optimum optimum, sum(log.val) sum from users
			join log on log.uid = users.id
			where date between datetime('now', 'start of month') and datetime('now', 'localtime')
			group by users.id
		",[]);

		while($row = $res->fetch(\PDO::FETCH_ASSOC)){
			$remainder = $row["border"] - $row["sum"];
			$remainder = ($remainder > 0 ? "+".round($remainder , 0) : round($remainder, 0));
			$class = ($remainder > 0 ? ($remainder >= $row["border"] - $row["optimum"] ? "val-pos" : "val-neu") : "val-neg");
			$cont .= "
				<tr>
					<td>{$row["name"]}</td>
					<td><span class=\"{$class}\">{$remainder}</span> czk</td>
				</tr>
			";
		}

		return $cont;
	}

	// return the total surplus (for each of two users)
	function get_surplus():string{
		$cont = "";
		$surplus = array(
			1 => array(
				"name" => "",
				"surplus" => 0
			),
			2 => array(
				"name" => "",
				"surplus" => 0
			)
		);
		$res = $this->db->query("
			select users.id id, users.name name, users.border border, SUM(log.val) sum from users
			join log on log.uid = users.id
			group by users.id, strftime('%m-%Y\', date)
		",[]);
		
		while($row = $res->fetch(\PDO::FETCH_ASSOC)){
			$surplus[$row["id"]]["name"] = $row["name"];
			$surplus[$row["id"]]["surplus"] = $row["border"] - $row["sum"] + $surplus[$row["id"]]["surplus"];
		}
		for($i=1;$i<=2;$i++){
			$surplus[$i]["surplus"] = ($surplus[$i]["surplus"] > 0 ? "+".round($surplus[$i]["surplus"], 0) : round($surplus[$i]["surplus"], 0));
			$class = ($surplus[$i]["surplus"] > 0 ? "val-pos" : "val-neg");
			$cont .= "
				<tr>
					<td>{$surplus[$i]["name"]}</td>
					<td><span class=\"{$class}\">{$surplus[$i]["surplus"]}</span> czk</td>
				</tr>
			";
		}

		return $cont;
	}

 	// return the number of rows
	function get_quantity():int{
		$res = $this->db->query("select count(*) count from log", []);
		return $res->fetch()["count"];
	}

}
