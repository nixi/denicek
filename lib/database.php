<?php
class database
{

	private $db;

	function __construct($file){
		try {
			$this->db = new PDO('sqlite:'.$file);
		} catch(PDOException $e) {
			error_log($e->getMessage(), 3, 'php://stdout');
		}
	}

	function query($sql, $attr){
		$stmt = $this->db->prepare($sql);
		$stmt->execute($attr);
		return $stmt;
	}

}
