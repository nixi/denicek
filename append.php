<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once("./lib/database.php");
require_once("./lib/logplay.php");

$db = new database("./db/log.db");
$log = new logplay($db);

if(!empty($_REQUEST["trip"]) && !empty($_REQUEST["loc"]) && !empty($_REQUEST["val"])){

	$trip = $_REQUEST["trip"];
	$loc = $_REQUEST["loc"];
	$val = $_REQUEST["val"];
	$desc = (empty($_REQUEST["desc"]) ? "" : $_REQUEST["desc"]);
	$date = (empty($_REQUEST["date"]) ? date("Y-m-d H:i:s") : DateTime::createFromFormat("Y-m-d", $_REQUEST["date"])->format("Y-m-d H:i:s"));

	if($uid = $log->auth($trip)){
		$log->append([$uid,$loc,$val,$desc,$date]);

		// returns an array with updated content of each ection
		// used for ajax append

		$c0 = $log->get_records(10);
		$c1 = $log->get_diff();
		$c2 = $log->get_remainder();
		$c3 = $log->get_surplus();
		$c4 = $log->get_quantity();

		$cont[] = array(
			"records" => $c0,
			"difference" => $c1,
			"remainder" => $c2,
			"surplus" => $c3,
			"quantity" => $c4
		);
		echo json_encode($cont);
		exit;
	}	
}
//header('Location: index.php');

