<?php
ini_set("display_errors", 1);

require_once("./lib/database.php");
require_once("./lib/logplay.php");

$db = new database("./db/log.db");
$log = new logplay($db);

include_once("./templates/index.html");

