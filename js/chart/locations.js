nv.addGraph(function() {
  var chart = nv.models.discreteBarChart()
      .x(function(d) { return d.label })    //Specify the data accessors.
      .y(function(d) { return d.value })
      .staggerLabels(true)    //Too many bars and not enough room? Try staggering labels.
      .tooltips(false)        //Don't show tooltips
      .showValues(true)       //...instead, show the bar value right on top of each bar.
      ;

  d3.select('#chart-locations svg')
      .datum(exampleData())
      .call(chart);

  nv.utils.windowResize(chart.update);

  return chart;
});

//Each bar represents a single discrete quantity.
function exampleData() {
 return  [ 
    {
      key: "Cumulative Return",
      values: [
        { 
          "label" : "tesco" ,
          "value" : 182
        } , 
        { 
          "label" : "Albert" , 
          "value" : 123
        } , 
        { 
          "label" : "Billa" , 
          "value" : 95
        } , 
        { 
          "label" : "Cuong" , 
          "value" : 80
        } 
      ]
    }
  ]

}

