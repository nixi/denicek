function append(){
	log = document.getElementById("log");
	difference = document.getElementById("difference");
	remainder = document.getElementById("remainder");
	surplus = document.getElementById("surplus");
	quantity = document.getElementById("quantity");

	trip = document.getElementsByName("trip")[0].value;
	loc = document.getElementsByName("loc")[0].value;
	val = document.getElementsByName("val")[0].value;
	date = document.getElementsByName("date")[0].value;
	desc = document.getElementsByName("desc")[0].value;

	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			result = this.responseText;
			obj = JSON.parse(result);
			log.innerHTML=obj[0].records;
			difference.innerHTML=obj[0].difference;
			remainder.innerHTML=obj[0].remainder;
			surplus.innerHTML=obj[0].surplus;
			quantity.innerHTML=obj[0].quantity;
		}
	}
	xmlhttp.open("GET","append.php?trip="+trip+"&loc="+loc+"&val="+val+"&date="+date+"&desc="+desc,true);
	xmlhttp.send();
}
