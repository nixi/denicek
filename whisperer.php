<?php
require_once("./lib/database.php");

$db = new database("./db/log.db");
$hint = (isset($_GET["hint"]) ? $_GET["hint"]."%" : "_%");
$cont = "";

$res = $db->query("
	select loc from log
	where loc like ?
	group by loc
	order by loc
", [$hint]);

while($row = $res->fetch(\PDO::FETCH_ASSOC)){
	$cont .= "<option value=\"".$row["loc"]."\">";
}

echo $cont;
